package skyscrapers.rulesSetByCell;


import gps.GPSEngine;
import gps.SearchStrategy;
import gps.api.GPSRule;
import skyscrapers.SKProblem;
import skyscrapers.SKState;

import java.util.Collections;
import java.util.LinkedList;

public class Main {

    public static void main(String[] args) {

        // Gets the initial state
        // Initializes Rules
        // Initializes problem
        // Calls solution

       // int[][] initialMatrix = {{0,2,0,2,1,0},{3,0,0,0,0,1},{1,0,0,0,0,4},{2,0,0,0,0,2},{3,0,0,0,0,2},{0,3,2,1,0,0}};
        int[][] initialMatrix = {{0,0,0,0,0},{0,0,0,0,0},{0,0,0,0,0},{0,0,0,0,0},{0,0,0,0,0}};
        //int[][] initialMatrix = {{0,4,2,3,1,0},{3,0,0,0,0,1},{2,0,0,0,0,3},{2,0,0,0,0,2},{1,0,0,0,0,2},{0,1,2,2,2,0},};

        LinkedList<GPSRule> rules = new LinkedList<>();

        int dim = initialMatrix.length - 2;

        for(int i = 1; i <= dim; i++){
            for(int j = 1; j <= dim; j++) {
                for(int k = 1; k <= dim; k++) {
                    rules.add(new SKRule(i,j,k));
                }
            }
        }

        Collections.shuffle(rules);

        SKProblem problem = new SKProblem(new SKState(initialMatrix), rules, new HBlankSpaces());

        GPSEngine engine = new GPSEngine(problem, SearchStrategy.IDDFS);

        engine.findSolution();

        System.out.println(engine.getSolutionNode());
    }
}
