package skyscrapers.rulesSetByCell;

import gps.api.GPSState;
import gps.api.HeuristicFunction;
import skyscrapers.SKState;

import java.util.ArrayList;
import java.util.List;

public class HHighestValuesPlus implements HeuristicFunction {
    @Override
    public Integer getHValue(GPSState state) {
        int[][] matrix = ((SKState) state).getMatrix();
        int length = ((SKState) state).getDim(), maxValue = length/2;
        int blankSpacesCounter = 0, highestValuesSum = 0;
        List<Integer> amounts = new ArrayList<>(((SKState) state).getDim());
        for (Integer i : amounts) {
            i = 0;
        }
        for(int i=1; i <= length; i++) {
            for (int j = 1; j <= length; j++) {
                if(matrix[i][j] == 0) {
                    blankSpacesCounter++;
                } else if(matrix[i][j] > maxValue) {
                    int current = amounts.get(matrix[i][j] - 1);
                    current++;
                    amounts.set(matrix[i][j] - 1, current);
                    highestValuesSum += matrix[i][j];
                }
            }
        }
        // total es la suma total de todos los valores del tablero si este estubiera lleno.
        int dim = ((SKState) state).getDim();
        int total = (dim * (dim + 1) / 2) * dim;
        int result1 = 0, m = dim;
        for (int i = 0; i < dim; i++) {
            result1 += amounts.get(i) * m;
            m--;
        }
        // ahora se hace una regla de 3 para cambiar la escala de total para que el resultado de la heuristica nunca sobreestime a h*
        double result = result1 * blankSpacesCounter / (double)total;

        return blankSpacesCounter - (int)result;
    }
}
