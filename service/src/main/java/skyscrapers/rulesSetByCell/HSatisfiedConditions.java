package skyscrapers.rulesSetByCell;

import gps.api.GPSState;
import gps.api.HeuristicFunction;
import skyscrapers.SKState;

/**
 * esta heuristica puede que no se admisible pero podera mas alto a aquellos tableros
 * que del total de restricciones ue hay cumplen con la mayor parte de ellas
 *
 */

public class HSatisfiedConditions implements HeuristicFunction{
    @Override
    public Integer getHValue(GPSState state) {
        SKState state1 = (SKState) state;
        int[][] matrix = state1.getMatrix();
        int dim = state1.getDim();
        int limitTop, limitBottom, limitRight, limitLeft, satisfiedConditions = 0;
        int totalConditions = dim * 4;
        int currentTop, currentBottom, currentRight, currentLeft;
        int counterTop = 1, counterBottom = 1, counterRight = 1, counterLeft = 1;
        for (int i = 1; i <= dim; i++) {
            limitTop = matrix[0][i];
            limitBottom = matrix[dim+1][i];
            limitLeft = matrix[i][0];
            limitRight = matrix[i][dim+1];
            currentTop = matrix[i][1];
            currentBottom = matrix[i][dim];
            currentRight = matrix[1][i];
            currentLeft = matrix[dim][i];
            for (int j = 1; j <= dim ; j++) {
                if(matrix[j][i] > currentTop) {
                    currentTop = matrix[j][i];
                    counterTop++;
                }
                if(matrix[dim + 1 - j][i] > currentBottom) {
                    currentBottom = matrix[dim + 1 - j][i];
                    counterBottom++;
                }
                if(matrix[i][j] > currentLeft) {
                    currentLeft = matrix[i][j];
                    counterLeft++;
                }
                if(matrix[i][dim + 1 - j] > currentRight) {
                    currentRight = matrix[i][dim + 1 - j];
                    counterRight++;
                }
            }

            if (counterBottom == limitBottom && limitBottom != 0) {
                satisfiedConditions++;
            } else if (limitBottom == 0) {
                totalConditions --;
            }
            if (counterTop == limitTop && limitTop != 0) {
                satisfiedConditions++;
            } else if (limitTop == 0) {
                totalConditions --;
            }
            if (counterLeft == limitLeft && limitLeft != 0) {
                satisfiedConditions++;
            } else if (limitLeft == 0) {
                totalConditions --;
            }
            if (counterRight == limitRight && limitRight != 0) {
                satisfiedConditions++;
            } else if (limitRight == 0) {
                totalConditions --;
            }
        }
        return totalConditions - satisfiedConditions;
    }
}
