package skyscrapers.rulesSetByCell;

import gps.api.GPSState;
import gps.api.HeuristicFunction;
import skyscrapers.SKState;

/**
 * Calculates de heuristic value depending on the
 * amount of blank spaces in the matrix of the given state.
 */
public class HBlankSpaces implements HeuristicFunction {

    @Override
    public Integer getHValue(GPSState state) {
        int[][] matrix = ((SKState) state).getMatrix();
        int length = ((SKState) state).getDim();
        int blankSpacesCounter = 0;

        for(int i=1; i <= length; i++) {
            for (int j = 1; j <= length; j++) {
                if(matrix[i][j] == 0) {
                    blankSpacesCounter++;
                }
            }
        }
        return blankSpacesCounter;
    }
}
