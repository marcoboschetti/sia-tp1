package skyscrapers.rulesSetByCell;


import gps.api.GPSRule;
import gps.api.GPSState;
import gps.exception.NotAppliableException;
import skyscrapers.SKState;

import java.awt.*;
import java.util.Arrays;
import java.util.Optional;

public class SKRule implements GPSRule {

    private int row,col,value;

    public SKRule(int row, int col, int value) {
        this.row = row;
        this.col = col;
        this.value = value;
    }

    @Override
    public Integer getCost() {
        return 1;
    }

    @Override
    public String getName() {
        return String.format("Building %d in (%d,%d)",value,row,col);
    }

    /**
     * A rule is appliable is there is no value in (row,col) of the matrix.
     * The inference engine will check for the board conditions
     * @param state
     *            The previous state of the problem.
     * @return
     * @throws NotAppliableException
     */
    @Override
    public Optional<GPSState> evalRule(GPSState state) {
        int [][] matrix = ((SKState)state).getMatrix();
        if (!contains(matrix) && isBlankSpace(matrix) && checkBorderConditions(matrix)) {
            int [][] newStateMatrix = cloneMatrix(((SKState)state).getMatrix());
            newStateMatrix[row][col] = value;
            SKState newState = new SKState(newStateMatrix);
            return Optional.of(newState);
        }
        return Optional.empty();
    }

    private int[][] cloneMatrix(int[][] matrix) {
        final int[][] ans = new int[matrix.length][];
        for (int i = 0; i < matrix.length; i++) {
            ans[i] = Arrays.copyOf(matrix[i], matrix[i].length);
        }

        return ans;
    }

    private boolean contains (final int[][] matrix) {
        for (int i = 1; i < matrix.length -1; i++) {
            if (value == matrix[i][col]) {
                return true;
            } else if (value == matrix[row][i]) {
                return true;
            }
        }
        return false;
    }

    public Point getCell(){
        return new Point(this.row, this.col);
    }

    private boolean isBlankSpace(final int[][] matrix) {
        return matrix[row][col] == 0;
    }

    private boolean checkBorderConditions(final int[][] matrix) {
        final int N = matrix.length;
        int rightCondition = matrix[row][N-1];
        int leftCondition = matrix[row][0];
        int topCondition = matrix[0][col];
        int bottomCondition = matrix[N-1][col];

        if (topCondition > 0) {
            int val = value + topCondition - (row - 1);
            if (val >= N) {
                return false;
            }
        }

        if (leftCondition > 0) {
            int val = value + leftCondition - (col - 1);
            if (val >= N) {
                return false;
            }
        }

        if (bottomCondition > 0) {
            int val = value + bottomCondition - (N - 2 - row);
            if (val >= N) {
                return false;
            }
        }

        if (rightCondition > 0) {
            int val = value + rightCondition - (N - 2 - col);
            if (val >= N) {
                return false;
            }
        }
        return true;
    }

}