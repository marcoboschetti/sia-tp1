package skyscrapers.rulesSetByCell;


import gps.api.GPSState;
import gps.api.HeuristicFunction;
import skyscrapers.SKState;

/**
 * Calculates the heuristic value as the maximum between the amount
 * of blank spaces on the matrix of the given state and the sum of
 * the values greater than the amount of rows of the same matrix.
 */
public class HHighestValues implements HeuristicFunction {

    @Override
    public Integer getHValue(GPSState state) {
        int[][] matrix = ((SKState) state).getMatrix();
        int length = ((SKState) state).getDim(), maxValue = length/2;
        int blankSpacesCounter = 0, highestValuesSum = 0;

        for(int i=1; i <= length; i++) {
            for (int j = 1; j <= length; j++) {
                if(matrix[i][j] == 0) {
                    blankSpacesCounter++;
                } else if(matrix[i][j] > maxValue) {
                    highestValuesSum += matrix[i][j];
                }
            }
        }
        // total es la suma total de todos los valores del tablero si este estubiera lleno.
        int dim = ((SKState) state).getDim();
        int total = (dim * (dim + 1) / 2) * dim;

        // ahora se hace una regla de 3 para cambiar la escala de total para que el resultado de la heuristica nunca sobreestime a h*
        double result = highestValuesSum * blankSpacesCounter / (double)total;

        return blankSpacesCounter - (int)result;
    }
}
