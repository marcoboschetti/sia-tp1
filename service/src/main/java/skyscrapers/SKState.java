package skyscrapers;


import gps.api.GPSState;

import java.util.Arrays;

public class SKState implements GPSState {

    private int[][] matrix;

    public SKState(int[][] matrix) {
        this.matrix = matrix;
    }

    public int getFromPosition(int x, int y) {
        return matrix[x][y];
    }

    public int getDim() {
        return matrix.length - 2;
    }

    public int[][] getMatrix() {
        return matrix;
    }

    @Override
    public String toString() {
        String ans = "";
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                ans += matrix[i][j] + "|";
            }
            ans += " \n-----------------\n";
        }
        ans += "\n\n\n";
        return ans;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SKState skState = (SKState) o;

        boolean equals = true;
        for (int i = 0; equals && i < matrix.length; ++i) {
            equals = Arrays.equals(this.matrix[i], skState.matrix[i]);
        }
        return equals;

    }

    @Override
    public int hashCode() {
        return Arrays.deepHashCode(matrix);
    }
}