package skyscrapers;


import gps.api.GPSProblem;
import gps.api.GPSRule;
import gps.api.GPSState;
import gps.api.HeuristicFunction;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SKProblem implements GPSProblem {

    private GPSState initialState;

    private List<GPSRule> rules;

    private HeuristicFunction heuristicFunction;


    public SKProblem(GPSState initialState, List<GPSRule> rules, HeuristicFunction heuristicFunction) {
        this.initialState = initialState;
        this.rules = rules;
        this.heuristicFunction = heuristicFunction;
    }

    @Override
    public GPSState getInitState() {
        return this.initialState;
    }

    @Override
    public boolean isGoal(GPSState state) {
        int [][] matrix = ((SKState)state).getMatrix();
        int dim = ((SKState)state).getDim();
        int borderConditionXLeft, borderConditionXRight, borderConditionYTop, borderConditionYBottom;
        int skyscraperSeenLeft = 0, skyscraperSeenRight = 0, skyscraperSeenTop = 0, skyscraperSeenBottom = 0;
        int currentValueXLeft, currentValueXRight, currentValueYTop, currentValueYBottom;
        int maxValueXLeft = 0, maxValueXRight = 0, maxValueYTop = 0, maxValueYBottom = 0;
        Set<Integer> usedNumbersXLeft, usedNumbersXRight, usedNumbersYTop, usedNumbersYBottom;

        for (int i=1; i <= dim; i++) {
            borderConditionXLeft = matrix[i][0];
            borderConditionXRight = matrix[i][dim+1];
            borderConditionYTop = matrix[0][i];
            borderConditionYBottom = matrix[dim+1][i];

            usedNumbersXLeft = new HashSet<>();
            usedNumbersXRight = new HashSet<>();
            usedNumbersYTop = new HashSet<>();
            usedNumbersYBottom = new HashSet<>();

            for (int j = 1; j <= dim ; j++) {
                currentValueXLeft = matrix[i][j];
                currentValueXRight = matrix[i][dim+1-j];
                currentValueYTop = matrix[j][i];
                currentValueYBottom = matrix[dim+1-j][i];

                // checks there are no blank spaces
                if (isBlankSpace(currentValueXLeft)
                        || isBlankSpace(currentValueXRight)
                        || isBlankSpace(currentValueYTop)
                        || isBlankSpace(currentValueYBottom)) {
                    return false;
                }

                // checks there are no repeated numbers
                if (usedNumbersXLeft.contains(currentValueXLeft)
                        || usedNumbersXRight.contains(currentValueXRight)
                        || usedNumbersYTop.contains(currentValueYTop)
                        || usedNumbersYBottom.contains(currentValueYBottom)) {
                    return false;
                }

                // checks border conditions
                if (borderConditionXLeft > 0 && maxValueXLeft < currentValueXLeft) {
                    maxValueXLeft = currentValueXLeft;
                    skyscraperSeenLeft++;
                    if (skyscraperSeenLeft > borderConditionXLeft) {
                        return false;
                    }
                }
                if (borderConditionXRight > 0 && maxValueXRight < currentValueXRight) {
                    maxValueXRight = currentValueXRight;
                    skyscraperSeenRight++;
                    if (skyscraperSeenRight > borderConditionXRight) {
                        return false;
                    }
                }
                if (borderConditionYTop > 0 && maxValueYTop < currentValueYTop) {
                    maxValueYTop = currentValueYTop;
                    skyscraperSeenTop++;
                    if (skyscraperSeenTop > borderConditionYTop) {
                        return false;
                    }
                }
                if (borderConditionYBottom > 0 && maxValueYBottom < currentValueYBottom) {
                    maxValueYBottom = currentValueYBottom;
                    skyscraperSeenBottom++;
                    if (skyscraperSeenBottom > borderConditionYBottom) {
                        return false;
                    }
                }

                usedNumbersXLeft.add(currentValueXLeft);
                usedNumbersXRight.add(currentValueXRight);
                usedNumbersYTop.add(currentValueYTop);
                usedNumbersYBottom.add(currentValueYBottom);
            }
        }
        return true;
    }

    private boolean isBlankSpace(int value) {
        return value == 0;
    }

    @Override
    public List<GPSRule> getRules() {
        return this.rules;
    }

    @Override
    public Integer getHValue(GPSState state) {
        return this.heuristicFunction.getHValue(state);
    }
}