package skyscrapers.rulesSetByRow;

import gps.api.GPSState;
import gps.api.HeuristicFunction;
import skyscrapers.SKState;

/**
 * Calculates de heuristic value depending on the
 * amount of blank spaces in the matrix of the given state.
 */
public class HBlankRows implements HeuristicFunction {

    @Override
    public Integer getHValue(GPSState state) {
        int[][] matrix = ((SKState) state).getMatrix();
        int length = ((SKState) state).getDim();
        int blankRows = 0;

        for(int i=1; i <= length; i++) {
            boolean isEmpty = true;
            for (int j = 1; j <= length && isEmpty; j++) {
                if(matrix[i][j] != 0) {
                    isEmpty = false;
                }
            }
            if(isEmpty){
                blankRows++;
            }
        }
        return blankRows;
    }
}
