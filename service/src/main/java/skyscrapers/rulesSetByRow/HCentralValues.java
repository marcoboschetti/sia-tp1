package skyscrapers.rulesSetByRow;

import gps.api.GPSState;
import gps.api.HeuristicFunction;
import skyscrapers.SKState;

public class HCentralValues implements HeuristicFunction {

    HBlankRows trivialHeuristic;

    public HCentralValues() {
        this.trivialHeuristic = new HBlankRows();
    }

    @Override
    public Integer getHValue(GPSState state) {
        int[][] matrix = ((SKState) state).getMatrix();
        int length = ((SKState) state).getDim();
        int movableRows = length;

        for(int i=0; i <= length; i++) {
            int leftSum = 0, centerSum = 0, rightSum = 0;
            for(int j=0; j <= length; j++) {
                if(j < length/4){
                    leftSum += matrix[i][j];
                }else if(j < length * 3 / 4){
                    centerSum += matrix[i][j];
                }else{
                    rightSum += matrix[i][j];
                }
            }
            if(centerSum > leftSum+rightSum){
                movableRows--;
            }
        }

        int trivialH = trivialHeuristic.getHValue(state);

        return Math.min(movableRows, trivialH);
    }

}
