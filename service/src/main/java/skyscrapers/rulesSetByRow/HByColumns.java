package skyscrapers.rulesSetByRow;

import gps.api.GPSState;
import gps.api.HeuristicFunction;
import skyscrapers.SKState;

public class HByColumns implements HeuristicFunction {
    @Override
    public Integer getHValue(GPSState state) {
        SKState skState = (SKState) state;
        int countDown = new HBlankRows().getHValue(state);
        double smaller = getSmallerAvg(avgByColumns(skState.getMatrix(), countDown));
        double scaleValue = (skState.getDim() - smaller) * countDown / skState.getDim();
        return (int)scaleValue;
    }

    private double[] avgByColumns(int[][] matrix, int depth) {
        double[] avg = new double[matrix.length - 2];
        for (int i = 1; i < matrix.length - 1; i++) {
            avg[i-1] = getColAvg(matrix, i, depth);
        }
        return avg;
    }

    private double getColAvg(int[][] matrix, int col, int depth) {
        int amount  = 0;
        for(int i = 1; i <= depth; i++) {
                amount += matrix[i][col];
        }
        return ((double)amount)/depth;
    }

    private double getBiggerAvg(double[] array) {
        double bigger = 0;
        for (int i = 0; i < array.length; i++) {
            if (bigger < array[i]) {
                bigger = array[i];
            }
        }
        return bigger;
    }

    private double getSmallerAvg(double[] array) {
        double bigger = Double.MAX_VALUE;
        for (int i = 0; i < array.length; i++) {
            if (bigger > array[i]) {
                bigger = array[i];
            }
        }
        return bigger;
    }

    private double getStartEndAvg(double[] avg) {
        return (avg[0] + avg[avg.length - 1]) / 2;
    }

    private double getCentralAvg(double[] avg) {
        int count = 0;
        double amount = 0;
        for(int i = 0; i < avg.length; i++) {
            if(!(i < avg.length / 4 || i > 3 * avg.length / 4)){
                count++;
                amount += avg[i];
            }
        }
        return amount/count;
    }


}
