package skyscrapers.rulesSetByRow;


import gps.api.GPSState;
import gps.api.HeuristicFunction;
import skyscrapers.SKState;

/**
 * Calculates de heuristic value as the maximum between the amount
 * of blank spaces on the matrix of the given state and the sum of
 * the values greater than the amount of rows of the same matrix.
 */
public class HContrainsMatch implements HeuristicFunction {

    HBlankRows trivialHeuristic;

    public HContrainsMatch() {
        this.trivialHeuristic = new HBlankRows();
    }

    @Override
    public Integer getHValue(GPSState state) {

        int[][] matrix = ((SKState) state).getMatrix();
        int length = ((SKState) state).getDim();
        int movableRows = length;

        for(int i=0; i <= length; i++) {
            if(matrix[0][i+1] == length-matrix[1][i+1]+1 || matrix[length+1][i+1] == length-matrix[length][i+1]+1
                    || matrix[i+1][0] == length-matrix[i+1][1]+1 || matrix[i+1][length+1] ==  length-matrix[i+1][length]+1){
                movableRows--;
            }
        }

        int trivialH = trivialHeuristic.getHValue(state);

        return Math.min(movableRows, trivialH);
    }
}
