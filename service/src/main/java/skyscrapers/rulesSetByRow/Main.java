package skyscrapers.rulesSetByRow;

import gps.GPSEngine;
import gps.SearchStrategy;
import gps.api.GPSRule;
import gps.api.HeuristicFunction;
import skyscrapers.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

public class Main {
    public static void main(String[] args) {

        final int dimension = 6;

        List<int[][]> testBoards = initailizeTestBoards();

        LinkedList<GPSRule> rules = new LinkedList<>();
        int dim = dimension - 2;
        List<Integer> init = getInitial(dim);
        List<List<Integer>> permutations = Permutations.generatePerm(init);
        for (int i = 1; i <= dim; i++) {
            for (List<Integer> integers : permutations) {
                int[] row = listToArray(integers);
                rules.add(new SKRule(i, row));
            }
        }

       // compareHeuristics(testBoards, rules, "stats-heuristics.csv");

       // compareStrategies(testBoards, rules, "stats-strategies.csv");

        compareShuffles(testBoards, rules, "stats-shuffle.csv");


    }

    private static List<int[][]> initailizeTestBoards() {
        //Easy-Med. Source: www.brainbashers.com
        int[][] initialMatrix0 = {{0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0}};
        int[][] initialMatrix1 = {{0, 2, 0, 2, 1, 0}, {3, 0, 0, 0, 0, 1}, {1, 0, 0, 0, 0, 4}, {2, 0, 0, 0, 0, 2}, {3, 0, 0, 0, 0, 2}, {0, 3, 2, 1, 0, 0}};
        int[][] initialMatrix2 = {{0, 4, 2, 3, 1, 0}, {3, 0, 0, 0, 0, 1}, {2, 0, 0, 0, 0, 3}, {2, 0, 0, 0, 0, 2}, {1, 0, 0, 0, 0, 2}, {0, 1, 2, 2, 2, 0}};
        int[][] initialMatrix3 = {{0, 3, 2, 1, 2, 0}, {3, 0, 0, 0, 0, 2}, {4, 0, 0, 0, 0, 1}, {2, 0, 0, 0, 0, 2}, {1, 0, 0, 0, 0, 2}, {0, 1, 2, 3, 2, 0}};
        int[][] initialMatrix4 = {{0, 2, 2, 3, 1, 0}, {2, 0, 0, 0, 0, 1}, {2, 0, 0, 0, 0, 2}, {3, 0, 0, 0, 0, 2}, {1, 0, 0, 0, 0, 3}, {0, 1, 3, 2, 3, 0}};
        int[][] initialMatrix5 = {{0, 3, 2, 1, 2, 0}, {3, 0, 0, 0, 0, 2}, {2, 0, 0, 0, 0, 3}, {1, 0, 0, 0, 0, 3}, {2, 0, 0, 0, 0, 1}, {0, 2, 3, 3, 1, 0}};

        //Hard
        int[][] initialMatrix6 = {{0, 2, 0, 0, 2, 0}, {0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 4}, {3, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0}};
        int[][] initialMatrix7 = {{0, 0, 0, 0, 2, 0}, {0, 0, 0, 0, 0, 0}, {2, 0, 0, 0, 0, 0}, {1, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0}, {0, 0, 3, 3, 0, 0}};
        int[][] initialMatrix8 = {{0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 4}, {0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0}, {3, 0, 0, 0, 0, 0}, {0, 0, 2, 0, 1, 0}};


        List<int[][]> testBoards = new LinkedList<>();
        testBoards.add(initialMatrix0);
        testBoards.add(initialMatrix1);
        testBoards.add(initialMatrix2);
        testBoards.add(initialMatrix3);
        testBoards.add(initialMatrix4);
        testBoards.add(initialMatrix5);
        testBoards.add(initialMatrix6);
        testBoards.add(initialMatrix7);
        testBoards.add(initialMatrix8);
        return testBoards;
    }


    public static void compareHeuristics(List<int[][]> testBoards, LinkedList<GPSRule> rules, String fileName) {
        //Compare heuristics
        HashMap<String, HeuristicFunction> heuristics = new HashMap<>();
        heuristics.put("Trivial", new HBlankRows());
        heuristics.put("Contrains Match", new HContrainsMatch());
        heuristics.put("Central values", new HCentralValues());
        heuristics.put("H by columns", new HByColumns());

        try {
            PrintWriter writerTimes = new PrintWriter("TIMES-"+fileName, "UTF-8");
            PrintWriter writerExplotions = new PrintWriter("EXPLOTIONS-"+fileName, "UTF-8");
            writerTimes.print("Heuristcs Times Comparison (ms)\nBoards,");
            writerExplotions.print("Heuristcs Expanded Nodes Comparison\nBoards,");
            for(int i = 0; i < testBoards.size(); i++) {
                writerTimes.print(String.format("Board %s,",""+i));
                writerExplotions.print(String.format("Board %s,",""+i));
            }
            writerTimes.println();
            writerExplotions.println();

            for (Map.Entry<String, HeuristicFunction> e : heuristics.entrySet()) {
                List<Long> times = new LinkedList();
                List<Long> expandedNodes = new LinkedList();

                for (int[][] board : testBoards) {
                    SKProblem problem = new SKProblem(new SKState(board), rules, e.getValue());
                    GPSEngine engine = new GPSEngine(problem, SearchStrategy.ASTAR);
                    long startTime = System.currentTimeMillis();
                    engine.findSolution();
                    long totalTime = System.currentTimeMillis() - startTime;
                    times.add(totalTime);
                    expandedNodes.add(engine.getExplosionCounter());
                }
                writerTimes.print(String.format("%s,", e.getKey()));
                writerExplotions.print(String.format("%s,", e.getKey()));
                for(int i = 0; i < times.size(); i++) {
                    writerTimes.print(String.format("%s,",""+times.get(i)));
                    writerExplotions.print(String.format("%s,",""+expandedNodes.get(i)));
                }
                writerTimes.println();
                writerExplotions.println();

            }

            writerTimes.close();
            writerExplotions.close();

        } catch (IOException e) {
            // do something
        }
    }

    public static void compareStrategies(List<int[][]> testBoards, LinkedList<GPSRule> rules, String fileName) {
        //Compare heuristics
        List<SearchStrategy> strategies = new LinkedList<>();
        strategies.add(SearchStrategy.BFS);
        strategies.add(SearchStrategy.DFS);
        strategies.add(SearchStrategy.IDDFS);
        strategies.add(SearchStrategy.GREEDY);
        strategies.add(SearchStrategy.ASTAR);


        try {
            PrintWriter writerTimes = new PrintWriter("TIMES-"+fileName, "UTF-8");
            PrintWriter writerExplotions = new PrintWriter("EXPLOTIONS-"+fileName, "UTF-8");
            writerTimes.print("Strategies Times Comparison (ms)\nBoards,");
            writerExplotions.print("Strategies Expanded Nodes Comparison\nBoards,");
            for(int i = 0; i < testBoards.size(); i++) {
                writerTimes.print(String.format("Board %s,",""+i));
                writerExplotions.print(String.format("Board %s,",""+i));
            }
            writerTimes.println();
            writerExplotions.println();

            for (SearchStrategy strategy : strategies) {
                List<Long> times = new LinkedList();
                List<Long> expandedNodes = new LinkedList();

                for (int[][] board : testBoards) {
                    SKProblem problem = new SKProblem(new SKState(board), rules, new HCentralValues());
                    GPSEngine engine = new GPSEngine(problem, strategy);
                    long startTime = System.currentTimeMillis();
                    engine.findSolution();
                    long totalTime = System.currentTimeMillis() - startTime;
                    times.add(totalTime);
                    expandedNodes.add(engine.getExplosionCounter());
                }
                writerTimes.print(String.format("%s,", strategy));
                writerExplotions.print(String.format("%s,",strategy));
                for(int i = 0; i < times.size(); i++) {
                    writerTimes.print(String.format("%s,",""+times.get(i)));
                    writerExplotions.print(String.format("%s,",""+expandedNodes.get(i)));
                }
                writerTimes.println();
                writerExplotions.println();

            }

            writerTimes.close();
            writerExplotions.close();

        } catch (IOException e) {
            // do something
        }
    }


    public static void compareShuffles(List<int[][]> testBoards, LinkedList<GPSRule> rules, String fileName) {

        boolean[] isShuffle = new boolean[]{false, true};




        try {
            PrintWriter writerTimes = new PrintWriter("TIMES-"+fileName, "UTF-8");
            PrintWriter writerExplotions = new PrintWriter("EXPLOTIONS-"+fileName, "UTF-8");
            writerTimes.print("Shuffle Times Comparison (ms)\nBoards,");
            writerExplotions.print("Shuffle Expanded Nodes Comparison\nBoards,");
            for(int i = 0; i < testBoards.size(); i++) {
                writerTimes.print(String.format("Board %s,",""+i));
                writerExplotions.print(String.format("Board %s,",""+i));
            }
            writerTimes.println();
            writerExplotions.println();

            for (boolean shuffle : isShuffle) {
                List<Long> times = new LinkedList();
                List<Long> expandedNodes = new LinkedList();

                for (int[][] board : testBoards) {
                    LinkedList<GPSRule> auxRules = new LinkedList<>(rules);
                    if (shuffle) {
                        Collections.shuffle(auxRules);
                    }

                    SKProblem problem = new SKProblem(new SKState(board), auxRules, new HCentralValues());
                    GPSEngine engine = new GPSEngine(problem, SearchStrategy.ASTAR);
                    long startTime = System.currentTimeMillis();
                    engine.findSolution();
                    long totalTime = System.currentTimeMillis() - startTime;
                    times.add(totalTime);
                    expandedNodes.add(engine.getExplosionCounter());
                }
                writerTimes.print(String.format("%s,", shuffle));
                writerExplotions.print(String.format("%s,",shuffle));
                for(int i = 0; i < times.size(); i++) {
                    writerTimes.print(String.format("%s,",""+times.get(i)));
                    writerExplotions.print(String.format("%s,",""+expandedNodes.get(i)));
                }
                writerTimes.println();
                writerExplotions.println();

            }

            writerTimes.close();
            writerExplotions.close();

        } catch (IOException e) {
            // do something
        }

    }



    private static List<Integer> getInitial(int dim) {
        List<Integer> list = new ArrayList<>();
        for (int i = 1; i <= dim; i++) {
            list.add(i);
        }
        return list;
    }

    private static int[] listToArray(List<Integer> integers) {
        int[] newArray = new int[integers.size()];
        for (int i = 0; i < integers.size(); i++) {
            newArray[i] = integers.get(i);
        }
        return newArray;
    }
}
