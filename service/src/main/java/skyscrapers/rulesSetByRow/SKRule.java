package skyscrapers.rulesSetByRow;

import gps.api.GPSRule;
import gps.api.GPSState;
import skyscrapers.SKState;

import java.util.Arrays;
import java.util.Optional;

public class SKRule implements GPSRule{

    private int row, rightValues, leftValues;
    private int[] value;

    public SKRule(int row, int[] value) {
        this.row = row;
        this.value = value;
        rightValues = getRowConditions(false);
        leftValues = getRowConditions(true);
    }

    @Override
    public Integer getCost() {
        return 1;
    }

    @Override
    public String getName() {
        return String.format("Building %s in (%d)",Arrays.toString(value),row);
    }

    public int getAffectedRow(){
        return row;
    }

    @Override
    public Optional<GPSState> evalRule(GPSState state) {
        int [][] matrix = ((SKState)state).getMatrix();
        if (checkBorderRLCondition(matrix) && rowIsEmpty(matrix) && !contains(matrix) && checkBorderRowConditions(matrix)) {
            int [][] newStateMatrix = cloneMatrix(((SKState)state).getMatrix());
            insertValues(newStateMatrix, value);
            SKState newState = new SKState(newStateMatrix);
            return Optional.of(newState);
        }
        return Optional.empty();
    }

    private boolean rowIsEmpty(final int[][] matrix) {
        for(int i = 1; i <= value.length; i++) {
             if(matrix[row][i] != 0) {
                 return false;
             }
        }
        return true;
    }

    private void insertValues(final int[][] newStateMatrix, final int[] value) {
        for(int i = 1; i <= value.length; i++) {
            newStateMatrix[row][i] = value[i-1];
        }
    }

    private boolean checkBorderRowConditions(final int[][] matrix) {
        for(int i = 1; i <= value.length; i++){
            if(!checkBorderConditions(matrix, i)) {
                return false;
            }
        }
        return true;
    }

    private boolean contains(final int[][] matrix) {
        for(int j = 1; j <= value.length; j++) {
            for (int i = 1; i < matrix.length -1; i++) {
                if (value[j-1] == matrix[i][j]) {
                    return true;
                }
            }
        }
        return false;
    }

    private int[][] cloneMatrix(final int[][] matrix) {
        final int[][] ans = new int[matrix.length][];
        for (int i = 0; i < matrix.length; i++) {
            ans[i] = Arrays.copyOf(matrix[i], matrix[i].length);
        }

        return ans;
    }


    private boolean checkBorderConditions(final int[][] matrix, final int col) {
        final int N = matrix.length;
        int topCondition = matrix[0][col];
        int bottomCondition = matrix[N-1][col];

        if (topCondition > 0) {
            int val = value[col-1] + topCondition - (row - 1);
            if (val >= N) {
                return false;
            }
        }

        if (bottomCondition > 0) {
            int val = value[col-1] + bottomCondition - (N - 2 - row);
            if (val >= N) {
                return false;
            }
        }

        return true;
    }

    private boolean checkBorderRLCondition(final int[][] matrix) {
        int N = matrix.length;
        int rightCondition = matrix[row][N-1];
        int leftCondition = matrix[row][0];
        if(rightCondition != 0 && rightCondition != rightValues) {
            return false;
        }
        if(leftCondition != 0 && leftCondition != leftValues) {
            return false;
        }
        return true;

    }

    /**
     *
     * @param direction false for right condition, true for left condition.
     * @return
     */

    private int getRowConditions(final boolean direction) {
        int factor, init;
        if(direction) {
            init = 0;
            factor = 1;
        } else {
            init = value.length - 1;
            factor = -1;
        }
        int currenti = 0,counti = 0;
        for(int i = init; i < value.length && i >= 0; i+=factor) {
            if (value[i] > currenti) {
                currenti = value[i];
                counti++;
            }
        }
        return counti;
    }
}
