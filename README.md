# Skyscrapers

### Description
The project solves the skyscrapers game using uninformed and informed search algorithms. These algorithms are: DFS, BFS, IDDFS, Greedy and A*.
It is possible to choose between different heuristic functions for using in the informed algorithms.

### Installing
* Download or save the skyscrapers.war in a folder.
* From a terminal run (folder_path is where the skyscrapers.war is stored):
```
$ cd folder_path
$ run skyscrapers.war // TODO --> poner cómo se va a correr
```
* Open a browser and in the address bar write: [localhost:8080](localhost:8080)
* An interface for running the algorithms with the desired initial configuration will start.

### Default initial configurations
It is possible to select predetermined initial configurations. For this, click on either of the *Load table X* buttons.