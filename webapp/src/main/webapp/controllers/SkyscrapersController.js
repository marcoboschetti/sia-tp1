angular.module('Skyscrapers', [])
    .controller('SkyscrapersController', function ($scope, $http) {


        $scope.problemData = {};
        $scope.problemData.fillOnes = true;
        $scope.problemData.strategy = 'BFS';
        $scope.problemData.heuristic = 'TRIVIAL';

        initPresets();

        $scope.tabState = 'dashboard';
        $scope.changeState = function (state) {
            $scope.tabState = state;
        };

        $scope.restartBoard = function (side) {
            $scope.problemSide = side;

            var board = [];
            for (var i = 0; i < side + 2; i++) {
                board[i] = {};
                board[i].cols = [];
                for (var j = 0; j < side + 2; j++) {
                    board[i].cols[j] = {};
                    if ((j == 0 || j == side + 1 || i == 0 || i == side + 1) && !((i == 0 || i == side + 1) && (j == 0 || j == side + 1))) {
                        board[i].cols[j].value = 'X';
                    } else {
                        board[i].cols[j].value = ' ';
                        board[i].cols[j].blocked = true;
                    }
                }
            }
            $scope.board = board;
        };

        $scope.restartBoard(4);
        $scope.solve = function () {
            $scope.serverError = false;
            var sides = [];
            sides[0] = {'row': []}; //UP
            sides[1] = {'row': []}; //LEFT
            sides[2] = {'row': []}; //DOWN
            sides[3] = {'row': []}; //RIGHT

            for (var i = 0; i < $scope.problemSide; i++) {
                sides[3].row.push($scope.board[i + 1].cols[0].value != 'X' ? $scope.board[i + 1].cols[0].value : 0);
                sides[1].row.push($scope.board[i + 1].cols[$scope.problemSide + 1].value != 'X' ? $scope.board[i + 1].cols[$scope.problemSide + 1].value : 0);

                sides[0].row.push($scope.board[0].cols[i + 1].value != 'X' ? $scope.board[0].cols[i + 1].value : 0);
                sides[2].row.push($scope.board[$scope.problemSide + 1].cols[i + 1].value != 'X' ? $scope.board[$scope.problemSide + 1].cols[i + 1].value : 0);
            }
            $scope.problemData.contrains = sides;

            $scope.isSolving = true;

            $http({
                method: 'POST',
                url: 'api/solve',
                data: JSON.stringify($scope.problemData)
            }).then(function (result) {
                console.log(result);
                $scope.solvedBoards = result.data.boards;
                $scope.currentSolvedBoard = 0;
                $scope.solution = result.data;

                $scope.isSolving = false;
                $scope.serverError = false;
                $scope.tabState = 'stats';
            }, function(result){
                console.log(result);
                $scope.serverError = true;
                $scope.isSolving = false;
            });
        };

        $scope.setCurrentSolvedBoard = function (newCurrentSolvedBoard) {
            if (newCurrentSolvedBoard < 0) {
                newCurrentSolvedBoard = 0;
            }
            if (newCurrentSolvedBoard >= $scope.solvedBoards.length) {
                newCurrentSolvedBoard = $scope.solvedBoards.length - 1;
            }
            $scope.currentSolvedBoard = newCurrentSolvedBoard;
            console.log($scope.solvedBoards[$scope.currentSolvedBoard])
        }

        function initPresets() {
            $scope.presets = [
                [[0, 0, 0, 0], [0, 0, 0, 0],  [0, 0, 0, 0], [0, 0, 0, 0]],
                [[2, 0, 2, 1], [3, 1, 2, 3],  [3, 2, 1, 0], [1, 4, 2, 2]],
                [[4, 2, 3, 1], [3, 2, 2, 1],  [1, 2, 2, 2], [1, 3, 2, 2]],
                [[3, 2, 1, 2], [3, 4, 2, 1],  [1, 2, 3, 2], [2, 1, 2, 2]],
                [[3, 2, 1, 2], [3, 2, 1, 2],  [2, 3, 3, 1], [2, 3, 3, 1]],
                [[2, 2, 3, 1], [2, 2, 3, 1],  [1, 3, 2, 3], [1, 2, 2, 3]],
                [[2, 0, 0, 2], [0, 0, 0, 3],  [0, 0, 0, 0], [0, 0, 4, 0]],
                [[0, 0, 0, 2], [0, 2, 1, 0],  [0, 3, 3, 0], [0, 0, 0, 0]],
                [[0, 0, 0, 0], [0, 0, 0, 3],  [0, 2, 0, 1], [4, 0, 0, 0]]
            ]
        }

        $scope.changePreset = function(presetString){
            var preset = JSON.parse(presetString);
            $scope.restartBoard(preset[0].length);

            for(var i = 0; i < preset[0].length; i++){
                $scope.board[0].cols[i+1].value = preset[0][i];
                $scope.board[preset[0].length+1].cols[i+1].value = preset[2][i];
                $scope.board[i+1].cols[0].value = preset[1][i];
                $scope.board[i+1].cols[preset[0].length+1].value = preset[3][i];
            }
        }
    });