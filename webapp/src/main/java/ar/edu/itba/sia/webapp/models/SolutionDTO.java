package ar.edu.itba.sia.webapp.models;

import gps.SearchStrategy;

import javax.ws.rs.core.GenericEntity;
import java.util.List;

public class SolutionDTO {

    //TO ADD
    // Heurística
    // Strategy
    // Tiempo de procesamiento
    // Profundidad de la solución (No pasar otra vez)
    // Costo obtenido de solucion (no pasar otra vez)
    // Cantidad de nodos frontera (los que quedaron en frontera)

    public List<BoardStateDTO> boards;
    public long expandedNodes;
    public String heuristicName;
    public SearchStrategy strategy;
    public long runningTimeMs;
    public long frontierNodes;
    public boolean failed;

    public SolutionDTO(List<BoardStateDTO> boards, long expandedNodes, String heuristicName, SearchStrategy strategy, long runningTimeMs, long frontierNodes, boolean failed) {
        this.boards = boards;
        this.expandedNodes = expandedNodes;
        this.heuristicName = heuristicName;
        this.strategy = strategy;
        this.runningTimeMs = runningTimeMs;
        this.frontierNodes = frontierNodes;
        this.failed = failed;
    }

    public SolutionDTO() {
    }
}
