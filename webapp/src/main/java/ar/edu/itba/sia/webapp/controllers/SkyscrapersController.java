package ar.edu.itba.sia.webapp.controllers;

import ar.edu.itba.sia.webapp.models.BoardStateDTO;
import ar.edu.itba.sia.webapp.models.ProblemParamsDTO;
import ar.edu.itba.sia.webapp.models.SolutionDTO;
import ar.edu.itba.sia.webapp.services.SkyscrapersService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/")
@Component
public class SkyscrapersController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SkyscrapersController.class);

    @Autowired
    private SkyscrapersService skyscrapersService;

    @POST
    @Path("/solve")
    @Produces(value = {MediaType.APPLICATION_JSON})
    public Response startProblem(ProblemParamsDTO paramsDTO) {
        SolutionDTO ans =  skyscrapersService.solveProblem(paramsDTO);
        return Response.ok(new GenericEntity<SolutionDTO>(ans){}).build();
    }

}

