package ar.edu.itba.sia.webapp.models;

/**
 * Created by Marco on 6/24/2017.
 */
public class CellDTO
{
    public int x;

    public int y;

    public CellDTO(double x, double y) {
        this.x = (int)x;
        this.y = (int)y;
    }
}
