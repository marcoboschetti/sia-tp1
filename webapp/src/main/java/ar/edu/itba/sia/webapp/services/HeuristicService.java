package ar.edu.itba.sia.webapp.services;


import gps.api.HeuristicFunction;
import org.springframework.stereotype.Controller;
import skyscrapers.rulesSetByCell.HBlankSpaces;
import skyscrapers.rulesSetByCell.HHighestValues;
import skyscrapers.rulesSetByCell.HSatisfiedConditions;

@Controller
public class HeuristicService {

    public HeuristicFunction stringToHeuristic(String heuristicName){
        switch (heuristicName){
            case "TRIVIAL":
                return  new HBlankSpaces();
            case "HIGHEST_VALUES":
                return  new HHighestValues();
            case "SATISFIED_CONDITIONS":
                return  new HSatisfiedConditions();
        }
        return null;
    }

}
