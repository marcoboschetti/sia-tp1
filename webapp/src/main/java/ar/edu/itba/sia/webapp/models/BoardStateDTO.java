package ar.edu.itba.sia.webapp.models;

import java.awt.*;

public class BoardStateDTO {
    public int[][] matrix;
    public String affectedCell;

    public BoardStateDTO() {
    }

    public BoardStateDTO(int[][] matrix, Point cell) {
        this.matrix = matrix;
        this.affectedCell = ((int)cell.getX())+","+ ((int)cell.getY());
    }
}
