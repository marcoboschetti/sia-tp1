package ar.edu.itba.sia.webapp.services;

import ar.edu.itba.sia.webapp.models.BoardStateDTO;
import ar.edu.itba.sia.webapp.models.ProblemParamsDTO;
import ar.edu.itba.sia.webapp.models.SolutionDTO;
import gps.GPSEngine;
import gps.GPSNode;
import gps.SearchStrategy;
import gps.api.GPSRule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import skyscrapers.SKProblem;
import skyscrapers.SKState;
import skyscrapers.rulesSetByCell.SKRule;

import java.awt.*;
import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

@Service
public class SkyscrapersService {

    @Autowired
    public HeuristicService heuristicService;

    public SolutionDTO solveProblem(ProblemParamsDTO paramsDTO){

        int[][] board = new int[paramsDTO.contrains.get(0).row.size()+2][paramsDTO.contrains.get(0).row.size()+2];

        List<String> top = paramsDTO.contrains.get(0).row;
        for(int i = 0; i < top.size(); i++) {
            board[0][i+1] = Integer.valueOf(top.get(i));
        }
        List<String> right = paramsDTO.contrains.get(1).row;
        for(int i = 0; i < right.size(); i++) {
            board[i+1][right.size()+1] = Integer.valueOf(right.get(i));
        }
        List<String> bottom = paramsDTO.contrains.get(2).row;
        for(int i = 0; i < bottom.size(); i++) {
            board[bottom.size()+1][i+1] = Integer.valueOf(bottom.get(i));
        }
        List<String> left = paramsDTO.contrains.get(3).row;
        for(int i = 0; i < left.size(); i++) {
            board[i+1][0] = Integer.valueOf(left.get(i));
        }

        LinkedList<GPSRule> rules = new LinkedList<>();

        int dim = board.length - 2;

        for(int i = 1; i <= dim; i++){
            for(int j = 1; j <= dim; j++){
                for(int k = 1; k <= dim; k++) {
                    rules.add(new SKRule(i,j,k));
                }
            }
        }

        SKProblem problem = new SKProblem(new SKState(board), rules, heuristicService.stringToHeuristic(paramsDTO.heuristic));

        GPSEngine engine = new GPSEngine(problem, SearchStrategy.valueOf(paramsDTO.strategy));

        long startTime = System.currentTimeMillis();
        engine.findSolution();
        long totalTime = System.currentTimeMillis() - startTime;

        if(engine.isFinished()){
            Deque<BoardStateDTO> list = new LinkedList<>();
            GPSNode node = engine.getSolutionNode();
            list.add(nodeToBoard(node));
            while((node = node.getParent()) != null){
                list.addFirst(nodeToBoard(node));
            }
            return new SolutionDTO((LinkedList)list, engine.getExplosionCounter(), paramsDTO.heuristic,engine.getStrategy(),totalTime,engine.getOpen().size(), engine.isFailed());
        }

        return new SolutionDTO(null, engine.getExplosionCounter(), paramsDTO.heuristic,engine.getStrategy(),totalTime,engine.getOpen().size(), engine.isFailed());

    }

    private BoardStateDTO nodeToBoard(GPSNode node) {
        if(node == null){
            return null;
        }

        int[][] matrix = ((SKState)node.getState()).getMatrix();

        SKRule appliedRule = ((SKRule)(node.getGenerationRule()));
        Point affectedCell = appliedRule == null? new Point(-1,-1) : appliedRule.getCell();
        return new BoardStateDTO(matrix, affectedCell);
    }

    private static List<Integer> getInitial(int dim) {
        List<Integer> list = new ArrayList<>();
        for (int i = 1; i <= dim; i++) {
            list.add(i);
        }
        return list;
    }

    private static int[] listToArray(List<Integer> integers) {
        int[] newArray = new int[integers.size()];
        for(int i = 0; i < integers.size(); i++) {
            newArray[i] = integers.get(i);
        }
        return newArray;
    }
}
